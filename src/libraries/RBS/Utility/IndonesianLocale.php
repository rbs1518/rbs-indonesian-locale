<?php
namespace RBS\Utility;
use Exception;
use DateTime;

/**
 * Class IndonesianLocale
 */
class IndonesianLocale
{
    /**
     * @var array
     */
    protected static $NumberWordDictionary = array(
        'Hypen' => ' ',
        'Conjunction' => ' ',
        'Separator' => ' ',
        'Negative' => 'negatif',
        'Decimal' => ' koma ',
        'OneHundredPrefix' => 'seratus',
        'OneThousandPrefix' => 'seribu',
        0 => 'nol',
        1 => 'satu',
        2 => 'dua',
        3 => 'tiga',
        4 => 'empat',
        5 => 'lima',
        6 => 'enam',
        7 => 'tujuh',
        8 => 'delapan',
        9 => 'sembilan',
        10 => 'sepuluh',
        11 => 'sebelas',
        12 => 'dua belas',
        13 => 'tiga belas',
        14 => 'empat belas',
        15 => 'lima belas',
        16 => 'enam belas',
        17 => 'tujuh belas',
        18 => 'delapan belas',
        19 => 'sembilan belas',
        20 => 'dua puluh',
        30 => 'tiga puluh',
        40 => 'empat puluh',
        50 => 'lima puluh',
        60 => 'enam puluh',
        70 => 'tujuh puluh',
        80 => 'delapan puluh',
        90 => 'sembilan puluh',
        100 => 'ratus',
        1000 => 'ribu',
        1000000 => 'juta',
        1000000000 => 'milyar',
        1000000000000 => 'triliun'
    );

    /**
     * @var array
     */
    protected static $DateNames = array(
        'D' => array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'),
        'M' => array(
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        )
    );

    /**
     * @param string|float|int $value
     * @return null|string
     * @throws Exception
     */
    public static function NumberToWords($value)
    {
        //From: http://www.karlrixon.co.uk/writing/convert-numbers-to-words-with-php/
        //Modified by rbs1518 to adapt common indonesian number words.
        $dict = self::$NumberWordDictionary;

        if (!is_numeric($value))
        {
            throw new Exception(__CLASS__ . ': Value is not numeric.');
        }

        if (($value >= 0 && (int)$value < 0) || (int)$value < 0 - PHP_INT_MAX)
        {
            throw new Exception(__CLASS__ . ': Value out of integer range.');
        }

        if ($value < 0)
        {
            return $dict['Negative'] . self::NumberToWords(abs($value));
        }

        $string = $fraction = null;
        if (strpos($value, '.') !== false)
        {
            list($value, $fraction) = explode('.', $value);
        }

        switch (true)
        {
            case $value < 21:
                $string = $dict[$value];
                break;
            case $value < 100:
                $tens = ((int)($value / 10)) * 10;
                $units = $value % 10;
                $string = $dict[$tens];
                if ($units)
                {
                    $string .= $dict['Hyphen'] . $dict[$units];
                }
                break;
            case $value < 1000:
                $hundreds = (int)($value / 100);
                $remainder = $value % 100;
                if ($hundreds < 2)
                {
                    $string = $dict['OneHundredPrefix'];
                }
                else
                {
                    $string = $dict[$hundreds] . ' ' . $dict[100];
                }
                if ($remainder)
                {
                    $string .= $dict['Conjunction'] . self::NumberToWords($remainder);
                }
                break;
            default:
                $baseUnit = pow(1000, floor(log($value, 1000)));
                $numBaseUnits = (int)($value / $baseUnit);
                $remainder = $value % $baseUnit;
                if (($baseUnit == 1000) && ($numBaseUnits == 1))
                {
                    $string = $dict['OneThousandPrefix'];
                }
                else
                {
                    $string = self::NumberToWords($numBaseUnits) . ' ' . $dict[$baseUnit];
                }
                if ($remainder)
                {
                    $string .= $remainder < 100 ? $dict['Conjunction'] : $dict['Separator'];
                    $string .= self::NumberToWords($remainder);
                }
                break;
        }

        if (null !== $fraction && is_numeric($fraction))
        {
            $string .= $dict['Decimal'];
            $words = array();
            foreach (str_split((string)$fraction) as $value)
            {
                $words[] = $dict[$value];
            }
            $string .= implode(' ', $words);
        }

        return $string;
    }

    /**
     * @param string|float|int $value
     * @return string
     */
    public static function ToCurrencyFormat($value)
    {
        $num = number_format($value, 2, ',', '.');
        return 'Rp. ' . $num;
    }

    /**
     * @param DateTime $dtObject
     * @return string
     */
    public static function ToFullDateString($dtObject)
    {
        if (is_string($dtObject))
        {
            $src = str_replace('-', ':', $dtObject);
            $dtObject = new DateTime($src);
        }

        if ($dtObject instanceof DateTime)
        {
            $ts = $dtObject->getTimestamp();
            $dp = getdate($ts);
            $na = self::$DateNames;
            return ($na['D'][$dp['wday']] . ', ' . $dp['mday'] . ' ' . $na['M'][$dp['mon'] - 1] . ' ' . $dp['year']);
        }
        return '';
    }

    /**
     * @param DateTime $dtObject
     * @return string
     */
    public static function ToLongDateString($dtObject)
    {
        if (is_string($dtObject))
        {
            $src = str_replace('-', ':', $dtObject);
            $dtObject = new DateTime($src);
        }

        if ($dtObject instanceof DateTime)
        {
            $ts = $dtObject->getTimestamp();
            $dp = getdate($ts);
            $na = self::$DateNames;
            return ($dp['mday'] . ' ' . $na['M'][$dp['mon'] - 1] . ' ' . $dp['year']);
        }
        return '';
    }
}