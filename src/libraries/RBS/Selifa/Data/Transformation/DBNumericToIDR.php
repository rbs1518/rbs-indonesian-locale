<?php
namespace RBS\Selifa\Data\Transformation;
use RBS\Selifa\Data\IRowTransformation;
use RBS\Utility\IndonesianLocale;

/**
 * Class DBNumericToIDR
 * @package MCU\Billing\Transformation
 */
class DBNumericToIDR implements IRowTransformation
{
    /**
     * @param mixed $value
     * @return string
     */
    public function ConvertTo($value)
    {
        return IndonesianLocale::ToCurrencyFormat($value);
    }

    /**
     * @param mixed $value
     * @return mixed
     */
    public function ConvertFrom($value)
    {
        return $value;
    }
}
?>